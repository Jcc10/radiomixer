interface AudioWorkletProcessor {
    readonly port: MessagePort;
    process(
        inputs: Float32Array[][],
        outputs: Float32Array[][],
        parameters: Record<string, Float32Array>
    ): boolean;
}

declare const AudioWorkletProcessor: {
    prototype: AudioWorkletProcessor;
    new(options?: AudioWorkletNodeOptions): AudioWorkletProcessor;
};

declare interface AudioParamDescriptor {
    name: string;
    automationRate?: "a-rate" | "k-rate";
    minValue?: number;
    maxValue?: number;
    defaultValue?: number;
}

declare function registerProcessor(
    name: string,
    processorCtor: (new (
        options?: AudioWorkletNodeOptions
    ) => AudioWorkletProcessor) & {
        parameterDescriptors?: AudioParamDescriptor[];
    }
): undefined;
declare let sampleRate: number;

const SMOOTHING_FACTOR = 0.9;
const MINIMUM_VALUE = 0.00001;
registerProcessor('vumeter', class extends AudioWorkletProcessor {
    private volume: number;
    private updateIntervalInMS: number;
    private nextUpdateFrame: number;

    constructor(options?: AudioWorkletNodeOptions) {
        super();
        this.volume = 0;
        this.updateIntervalInMS = options?.processorOptions?.updateIntervalInMS | 25;
        this.nextUpdateFrame = this.updateIntervalInMS;
        this.port.onmessage = event => {
            if (event.data.updateIntervalInMS)
                this.updateIntervalInMS = event.data.updateIntervalInMS;
        }
    }

    get intervalInFrames() {
        return this.updateIntervalInMS / 1000 * sampleRate;
    }

    process(
        inputs: Float32Array[][],
        _outputs: Float32Array[][],
        _parameters: Record<string, Float32Array>
    ): boolean {
        const input = inputs[0];
        // Note that the input will be down-mixed to mono; however, if no inputs are
        // connected then zero channels will be passed in.
        if (input.length > 0) {
            const samples = input[0];
            let sum = 0;
            let rms = 0;

            // Calculated the squared-sum.
            for (let i = 0; i < samples.length; ++i) {
                sum += samples[i] * samples[i];
            }
            // Calculate the RMS level and update the volume.
            rms = Math.sqrt(sum / samples.length);
            this.volume = Math.max(rms, this.volume * SMOOTHING_FACTOR);

            // Update and sync the volume property with the main thread.
            this.nextUpdateFrame -= samples.length;
            if (this.nextUpdateFrame < 0) {
                this.nextUpdateFrame += this.intervalInFrames;
                this.port.postMessage({ volume: this.volume });
            }
        }
        // Keep on processing if the volume is above a threshold, so that
        // disconnecting inputs does not immediately cause the meter to stop
        // computing its smoothed value.
        return this.volume >= MINIMUM_VALUE;
    }
});