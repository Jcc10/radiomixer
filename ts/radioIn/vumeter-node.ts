export default class VUMeterNode extends AudioWorkletNode {
    private updateIntervalInMS: number;
    private volumeInt: number;
    constructor(context: BaseAudioContext, updateIntervalInMS: number) {
        super(context, 'vumeter', {
            numberOfInputs: 1,
            numberOfOutputs: 0,
            channelCount: 1,
            processorOptions: {
                updateIntervalInMS: updateIntervalInMS || 16.67
            }
        });

        // States in AudioWorkletNode
        this.updateIntervalInMS = updateIntervalInMS;
        this.volumeInt = 0;

        // Handles updated values from AudioWorkletProcessor
        this.port.onmessage = event => {
            if (event.data.volume)
                this.volumeInt = event.data.volume;
        }
        this.port.start();
    }
    get updateInterval() {
        return this.updateIntervalInMS;
    }

    set updateInterval(updateIntervalInMS: number) {
        this.updateIntervalInMS = updateIntervalInMS;
        this.port.postMessage({ updateIntervalInMS: updateIntervalInMS });
    }

    get volume() {
        return this.volumeInt;
    }
}