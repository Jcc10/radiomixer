import VUMeterNode from './vumeter-node.ts';
const context = new AudioContext();

export class RadioIn {
    lastActive = -Infinity;
    lastVol = 0;
    skipCount = 0;
    triggerVol = 1;
    radioActive = () => { };
    context: AudioContext | null = null;
    stream: MediaStream | null = null;
    halt = false;
    mic = ""
    speak = ""
    onFrame = () => { };
    constructor() {
    }
    setDevice(mic: string, speak: string) {
        this.mic = mic;
        this.speak = speak;
    }
    async setup() {
        if (this.context) {
            this.context.resume();
            this.halt = false;
            globalThis.requestAnimationFrame(this.onFrame);
        } else {
            this.halt = false;
            this.stream = await navigator.mediaDevices.getUserMedia({
                audio: {
                    deviceId: this.mic,
                },
                video: false,
            });
            const audioContext = this.context = new AudioContext();
            const mediaStreamSource = audioContext.createMediaStreamSource(
                this.stream,
            );

            mediaStreamSource.connect(audioContext.destination);
            await context.audioWorklet.addModule('vumeter-processor.js');
            const vuMeterNode = new VUMeterNode(context, 25);
            mediaStreamSource.connect(vuMeterNode);
            this.onFrame = () => {
                if (this.halt) {
                    return;
                }
                if (this.skipCount >= 2) {
                    this.skipCount = 0;
                } else {
                    this.skipCount++;
                    globalThis.requestAnimationFrame(this.onFrame);
                }
                const now = Date.now();
                if (vuMeterNode.volume > this.triggerVol) {
                    if (now - this.lastActive > 5000) {
                        this.radioActive();
                    }
                    this.lastActive = now;
                } else {
                    if (now - this.lastActive > 5000) {
                        this.lastActive = -Infinity;
                    }
                }
                requestAnimationFrame(this.onFrame);
            }
            this.onFrame();
        }
    }
    stop() {
        this.context?.suspend();
        this.halt = true;
    }
    kill() {
        this.onFrame = () => { };
        this.halt = true
        this.context?.close();
    }
}
