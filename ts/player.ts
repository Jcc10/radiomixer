const mediaCacheName = "media";
const mediaCache: Cache = await caches.open(mediaCacheName);

function randRange(max: number, min = 0) {
  return Math.floor((Math.random() * (max - min)) + min);
}

export type track = { src: string, name: string, by: string, skip?: true }
export class Player {
  tracks: Array<track>;
  history: Array<number> = [];
  autoQueue = 0;
  audioElem: HTMLAudioElement;
  display: string;
  state: "playing" | "paused" = "playing";
  albums = new Map<string, Map<string, number>>();
  ended = () => { };
  error = () => { };
  constructor(audioElem: HTMLAudioElement, tracks: Array<track>) {
    this.tracks = tracks;
    this.audioElem = audioElem;
    this.audioElem.addEventListener("ended", () => this.ended());
    this.audioElem.addEventListener("error", () => this.error());
    this.display = "";
    this.audioElem.volume = 0.50;
    const ac = this.albums;
    for (let id = 0; id < this.tracks.length; id++) {
      const track = this.tracks[id];
      let album = ac.get(track.by)
      if (!album) {
        album = new Map<string, number>();
        ac.set(track.by, album);
      }
      album.set(track.name, id);
    }
    this.randomTrack();
  }
  get volume() {
    return this.audioElem.volume;
  }
  set volume(n: number) {
    if (n > 1)
      n = 1;
    if (n < 0)
      n = 0;
    this.audioElem.volume = n;
  }
  get pos(): [number, number] {
    return [this.audioElem.currentTime, this.audioElem.duration];
  }
  changeTrack() {
    if (this.history.length == 0) {
      return;
    }
    const track = this.tracks[this.history[this.history.length - 1]]
    this.audioElem.src = track["src"];
    this.display = `Now playing: ${track["by"]}; ${track["name"]}`;
    this.audioElem.currentTime = 0;
  }
  lastTwo() {
    let a = -1;
    if (this.history.length >= 1) {
      a = this.history[this.history.length - 1];
    }
    let b = -1;
    if (this.history.length >= 2) {
      b = this.history[this.history.length - 2];
    }
    return [
      a,
      b,
    ]
  }
  randomTrack() {
    const lastRandom = this.autoQueue;
    let foundTrack: number | null = null;
    let failed = 0;
    const lt = this.lastTwo()
    while (foundTrack === null) {
      const rand = randRange(this.tracks.length - 1);
      const track = this.tracks[rand];
      if (
        !track.skip &&
        rand != lt[0] &&
        rand != lt[1]
      ) {
        foundTrack = rand;
        break;
      }
      failed++;
      if (failed > 3) {
        foundTrack = rand;
      }
    }
    this.autoQueue = foundTrack;
    mediaCache.add(this.tracks[this.autoQueue].src);
    return lastRandom;
  }
  next(trackID?: number) {
    let foundTrack: number;
    if (trackID && trackID >= 0 && trackID < this.tracks.length) {
      foundTrack = trackID;
    } else {
      foundTrack = this.randomTrack();
    }
    this.history.push(foundTrack);
    this.changeTrack();
  }
  last() {
    if (this.history.length < 2) {
      this.next();
      return;
    }
    this.history.pop();
    this.changeTrack();
  }
  pause() {
    this.audioElem.pause();
    this.state = "paused";
  }
  hold() {
    this.audioElem.pause();
  }
  restore() {
    if (this.state == "playing") {
      this.play();
    }
  }
  play() {
    this.audioElem.play();
    this.state = "playing";
  }
  pausedOrDone() {
    return this.audioElem.paused || this.audioElem.ended
  }
  delta(time: number) {
    this.audioElem.currentTime += time;
  }
  fastDelta(time: number) {
    this.audioElem.fastSeek(this.audioElem.currentTime + time);
  }
}
