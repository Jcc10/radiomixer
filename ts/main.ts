/// <reference lib="dom" />
import type { } from "https://cdn.skypack.dev/@types/dom-screen-wake-lock?dts";
import { Ring } from "./ring.ts";
import { RadioIn } from "./radioIn/radioIn.ts";
import { Player } from "./player.ts";
import { tracks } from "../media/tracks.ts";
import type { Menu } from "./menus/menuAbs.ts";
import { Menu as MainMenu } from "./menus/mainMenu.ts";
import { Menu as PlayerMenu } from "./menus/playerMenu.ts";
import { Menu as DummyMenu } from "./menus/dummyMenu.ts";
import { Menu as InfoMenu } from "./menus/infoMenu.ts";
import { Menu as VoxCaliMenu } from "./menus/voxCaliMenu.ts";
import { nonce } from "./nonce.ts";
const cacheName = "radio_mixer";
const output = document.getElementById("Output1")!;

function evn(text: string) {
  output.innerHTML = text;
}

const globalSynth = globalThis.speechSynthesis;
const menuStack: Array<Menu> = [];
function topMenu(): Menu {
  return menuStack[menuStack.length - 1];
}
const mainMenu = new MainMenu(globalSynth, menuStack);
const player = new Player(
  document.getElementById("audio") as HTMLAudioElement,
  tracks,
);
const infoMenu = new InfoMenu(globalSynth, menuStack);
const radioIn = new RadioIn();
const voxCaliMenu = new VoxCaliMenu(globalSynth, menuStack, radioIn);
const musicPlayer = new PlayerMenu(globalSynth, menuStack, player);
const bookPlayer = new DummyMenu(globalSynth, menuStack);
const bambiPlayer = new DummyMenu(globalSynth, menuStack);
mainMenu.addMenu(infoMenu, "Info");
mainMenu.addMenu(voxCaliMenu, "VOX Calibration");
mainMenu.addMenu(musicPlayer, "Music Player");
mainMenu.addMenu(bookPlayer, "Book Menu");
mainMenu.addMenu(bambiPlayer, "Bambi Player");

menuStack.push(mainMenu);

// Ring code for folding.
{
  const ring = new Ring(
    document.getElementById("swiper")!,
  );
  let active = false;
  ring.diag = (diag: unknown) => {
    const s = diag as string;
    evn(s);
  }
  ring.up1 = async () => {
    if (active) {
      return;
    }
    active = true;
    await topMenu().up1();
    active = false;
  };
  ring.up2 = async () => {
    if (active) {
      return;
    }
    active = true;
    await topMenu().up2();
    active = false;
  };
  ring.center1 = async () => {
    if (active) {
      return;
    }
    active = true;
    await topMenu().center();
    active = false;
  };
  ring.center2 = async () => {
    if (active) {
      return;
    }
    active = true;
    await topMenu().leave()
    active = false;
  };
  ring.down1 = async () => {
    if (active) {
      return;
    }
    active = true;
    await topMenu().down1();
    active = false;
  };
  ring.down2 = async () => {
    if (active) {
      return;
    }
    active = true;
    await topMenu().down2();
    active = false;
  };
  ring.other = () => {
  };
}

radioIn.radioActive = () => {
  evn("Radio Went Active!");
  topMenu().mute();
};

function _deleteAllCookies() {
  const cookies = document.cookie.split(";");

  for (let i = 0; i < cookies.length; i++) {
    const cookie = cookies[i];
    const eqPos = cookie.indexOf("=");
    const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
    document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
  }
}

document.getElementById("reenter")!.addEventListener("click", () => {
  topMenu().jumpStart();
});
document.getElementById("reload")!.addEventListener("click", async () => {
  console.log("Reloading!");
  //evn("Clearing Cookies!");
  //deleteAllCookies();
  evn("Clearing Cache!");
  document.getElementById("reload")!.style.backgroundColor = "yellow";
  const cache = await caches.open(cacheName);
  const filesToCache = [
    "/",
    "/index.html",
    "/css/style.css",
    "/js/main.js",
    "/hash",
  ];
  for (const file of filesToCache) {
    await cache.delete(file);
  }
  cache.addAll(filesToCache);
  document.getElementById("reload")!.style.backgroundColor = "green";
  evn("Reloading!");
  location.reload();
});


const micSelector = document.getElementById("micSelector")! as HTMLSelectElement;
const speakerSelector = document.getElementById("speakerSelector")! as HTMLSelectElement;
const micLockButton = document.getElementById("miclock")!;
const deviceMenu = document.getElementById("deviceMenu")!;
micLockButton.addEventListener("click", async () => {
  if (deviceMenu.style.display == "none") {
    const devices = await navigator.mediaDevices.enumerateDevices();
    let count = 0;
    for (const device of devices) {
      if (device.kind == "audioinput") {
        const opt1 = document.createElement("option");
        opt1.value = device.deviceId;
        opt1.innerHTML = device.label;
        console.log(device.label)
        micSelector.appendChild(opt1);
        const opt2 = document.createElement("option");
        opt2.value = device.deviceId;
        opt2.innerHTML = device.label + " - I";
        console.log(device.label)
        speakerSelector.appendChild(opt2);
        count++
      }
      if (device.kind == "audiooutput") {
        const opt2 = document.createElement("option");
        opt2.value = device.deviceId;
        opt2.innerHTML = device.label + " - O";
        console.log(device.label)
        speakerSelector.appendChild(opt2);
        count++
      }
    }
    evn(`Found ${devices.length} audio Devices.`)
    deviceMenu.style.display = "";
  } else {
    const mic = micSelector.options[micSelector.selectedIndex]?.value;
    const speak = speakerSelector.options[micSelector.selectedIndex]?.value;
    deviceMenu.style.display = "none";
    radioIn.setDevice(mic, speak);
    evn(`Mic ${micSelector.options[micSelector.selectedIndex]?.text} now overrides.`);
  }
});



const wakeLockButton = document.getElementById("wakelock")!;
document.getElementById("stopRadio")!.addEventListener("click", () => {
  radioIn.stop();
  wakeLockButton.innerHTML = "⚠";
  try {
    document.querySelector("body")!.requestFullscreen();
    navigator.wakeLock.request("screen")
    window.screen?.orientation?.lock("portrait-primary");
  } catch (e) {
    printStatus(e.text);
  }
});
wakeLockButton.addEventListener("click", () => {
  evn("Toggleing Wakelock...");
  toggleWake();
});

function printStatus(status: string) {
  evn("WLE: " + status);
}

let wakeLockObj: WakeLockSentinel | null = null;

function toggleWake() {
  if ("keepAwake" in screen) {
    printStatus("didn't feel like setting up keepAwake :(");
  } else if ("wakeLock" in navigator) {
    if (wakeLockObj) {
      wakeLockObj.release();
      wakeLockObj = null;
      window.screen?.orientation?.unlock();
      document.exitFullscreen();
      wakeLockButton.innerHTML = "🚫";
      printStatus("released");
      radioIn.stop();
    } else {
      printStatus("acquiring...");
      document.querySelector("body")!.requestFullscreen();
      navigator.wakeLock.request("screen")
        .then((wakeLock) => {
          wakeLockObj = wakeLock;

          wakeLockObj.addEventListener("release", () => {
            printStatus("released externally");
            window.screen?.orientation?.unlock();
            document.exitFullscreen();
            wakeLockButton.innerHTML = "⚠";
            wakeLockObj = null;
          });
          radioIn.setup();
          printStatus("acquired");
          try {
            window.screen?.orientation?.lock("portrait-primary");
          } catch (e) {
            printStatus(e.text);
          }
          wakeLockButton.innerHTML = "✔";
        })
        .catch((err) => {
          console.error(err);
          printStatus("failed to acquire: " + err.message);
        });
    }
  }
}

if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("./sw.js").then(function (registration) {
      console.log(
        "ServiceWorker registration successful with scope: ",
        registration.scope,
      );
    }, function (err) {
      console.log("ServiceWorker registration failed: ", err);
    });
}

fetch("/hash").then(res => res.text()).then(
  (hash) => {
    mainMenu.hash = nonce;
    mainMenu.hash = hash;
    output.innerHTML = `V0.3.0-${hash}`
    document.getElementById("hash")!.innerHTML = hash
    mainMenu.jumpStart();
  }
)