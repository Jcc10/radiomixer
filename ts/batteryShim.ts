export interface BatteryManager extends EventTarget {
    readonly charging: boolean;
    readonly chargingTime: number;
    readonly dischargingTime: number;
    readonly level: number;
}
declare global {
    interface Navigator {
        getBattery: () => Promise<BatteryManager>;
    }
}

export function getBattery() {
    return navigator.getBattery();
}