/// <reference lib="dom" />
//import ZingTouch from "https://cdn.skypack.dev/zingtouch?dts";
// Import is causing crashes when bundeling, let's try doing this manually.

interface Point {
  x: number,
  y: number,
}
function deltaDeg(p1: Point, p2: Point) {
  return Math.atan2(p2.y - p1.y, p2.x - p1.x) * 180 / Math.PI;
}
export class Ring {
  up1 = () => { };
  up2 = () => { };
  center1 = () => { };
  center2 = () => { };
  down1 = () => { };
  down2 = () => { };
  other = () => { };
  diag = (_dag: unknown) => { };
  private actuallyDouble = false;

  constructor(targetElement: HTMLElement) {
    targetElement.addEventListener("click", (_e) => {
      this.actuallyDouble = false;
      setTimeout(() => {
        if (this.actuallyDouble) {
          return;
        } else {
          this.center1();
        }
      }, 125);
    })
    targetElement.addEventListener("dblclick", (_e) => {
      this.actuallyDouble = true;
      this.center2();
    })
    targetElement.addEventListener("touchstart", (e) => {
      this.touchStart(e)
    })
    targetElement.addEventListener("touchmove", (e) => {
      this.touchMove(e)
    })
    targetElement.addEventListener("touchend", (e) => {
      this.touchEnd(e)
    })
  }

  private start: Point | null = null;
  private last: Point | null = null;
  private touchStart(e: TouchEvent) {
    const t = e.touches[0];
    this.start = { x: t.clientX, y: t.clientY }
    this.last = null;
  }
  private touchMove(e: TouchEvent) {
    const t = e.touches[0];
    this.last = { x: t.clientX, y: t.clientY }
  }
  private touchEnd(_e: TouchEvent) {
    const p1 = this.start;
    const p2 = this.last;
    if (!p1 || !p2) {
      return;
    }
    this.parseDrag(deltaDeg(p1, p2))
    this.start = null;
    this.last = null;
  }
  private parseDrag(deg: number) {
    switch (deg) {
      case -90:
        this.up1();
        break;
      case 0:
        this.up2();
        break;
      case 90:
        this.down1();
        break;
      case 180:
        this.down2();
        break;
      default:
        this.other();
        break;
    }
  }
}