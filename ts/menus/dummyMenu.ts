import { Menu as AbsMenu } from "./menuAbs.ts";

export class Menu extends AbsMenu {
    enter(): void | Promise<void> {
        this.speak("Dummy Menu");
        this.d1("Dummy Menu")
    }
    up1(): void | Promise<void> {
        this.speak("Dummy Menu Up 1");
    }
    up2(): void | Promise<void> {
        this.speak("Dummy Menu Up 2");
    }
    down1(): void | Promise<void> {
        this.speak("Dummy Menu Down 1");
    }
    down2(): void | Promise<void> {
        this.speak("Dummy Menu Down 2");
    }
    center(): void | Promise<void> {
        this.speak("Dummy Menu Select");
    }
    leave(): void | Promise<void> {
        this.leaveHelper();
    }
    mute(): void | Promise<void> {
        this.synth.pause();
    }

}