import { Menu as AbsMenu } from "./menuAbs.ts";
import { RadioIn } from "../radioIn/radioIn.ts";

export class Menu extends AbsMenu {
    radio: RadioIn;
    constructor(globalSynth: SpeechSynthesis, menuStack: Array<AbsMenu>, radioIn: RadioIn) {
        super(globalSynth, menuStack);
        this.radio = radioIn;
    }
    enter(): void | Promise<void> {
        this.d1("VOX Menu")
        this.speak("VOX Calibration");
    }
    up1(): void | Promise<void> {
        this.d2(`Current: ${this.radio.triggerVol}`);
        this.speak(`Current: ${this.radio.triggerVol}`);
    }
    up2(): void | Promise<void> {
        this.d2(`Current: ${this.radio.triggerVol}`);
        this.speak(`Current: ${this.radio.lastActive == -Infinity ? "Inactive" : "Active"}`);
    }
    down1(): void | Promise<void> {
        const setting = this.radio.lastVol
        this.d2(`Set Actual:`);
        this.d3(`${setting}`);
        this.radio.triggerVol = setting;
        this.speak(`Set Actual: ${setting}`);
    }
    down2(): void | Promise<void> {
        const setting = this.radio.lastVol * 0.75;
        this.d2(`Set .75: ${setting}`);
        this.d3(`${setting} / ${this.radio.lastVol}`);
        this.radio.triggerVol = setting;
        this.speak(`Set point 7 5: ${setting}`);
    }
    center(): void | Promise<void> {
        this.d2(`Live: ${this.radio.lastVol}`);
        this.speak(`Live: ${this.radio.lastVol}`);
    }
    leave(): void | Promise<void> {
        this.leaveHelper();
    }
    mute(): void | Promise<void> {
        this.synth.pause();
    }

}