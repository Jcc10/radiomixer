import { Menu as AbsMenu } from "./menuAbs.ts";

export abstract class RotateMenuAbs extends AbsMenu {
    options: Array<string> = [];
    selected = 0;

    selText(): string {
        return this.options[this.selected];
    }
    // deno-lint-ignore no-explicit-any
    enter(..._args: any): void | Promise<void> {
        this.selected = 0;
    }
    // deno-lint-ignore no-explicit-any
    up1(..._args: any): void | Promise<void> {
        this.selected++;
        if (this.selected >= this.options.length) {
            this.selected = 0;
        }
        const name = this.selText();
        this.speak(name);
        this.d2(name)
    }
    // deno-lint-ignore no-explicit-any
    down1(..._args: any): void | Promise<void> {
        this.selected--;
        if (this.selected < 0) {
            this.selected = this.options.length - 1;
        }
        const name = this.selText();
        this.speak(name);
        this.d2(name)
    }
    // deno-lint-ignore no-explicit-any
    down2(..._args: any): void | Promise<void> {
        const name = this.selText();
        this.speak(name);
        this.d2(name)
    }
    // deno-lint-ignore no-explicit-any
    mute(..._args: any): void | Promise<void> {
        this.synth.pause();
    }

}