//import { Menu as AbsMenu } from "./menuAbs.ts";
import { Menu as RotateMenuAbs } from "./rotateMenuAbs.ts";

class Timer {

}

class _ShiftTimer extends Timer {

}

export class Menu extends RotateMenuAbs {
    options: Array<string> = ["Break", "Lunch"];
    timers: Array<Timer> = [];
    enter(): void | Promise<void> {
        this.speak("Timer Menu");
        this.d1("Timer Menu")
        this.d2(`Timer ${this.selText()}`)
    }
    up2(): void | Promise<void> {
        this.speak("Dummy Menu Up 2");
    }
    down2(): void | Promise<void> {
        this.speak("Dummy Menu Down 2");
    }
    center(): void | Promise<void> {
    }
    leave(): void | Promise<void> {
        this.leaveHelper();
    }
    mute(): void | Promise<void> {
        this.synth.pause();
    }

}