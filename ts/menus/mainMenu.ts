import { Menu as AbsMenu } from "./menuAbs.ts";
import { RotateMenuAbs } from "./selectionMenusAbs.ts";
export class Menu extends RotateMenuAbs {
    menuWheel: Array<AbsMenu> = [];
    options: Array<string> = [];
    hash = "Unknown";

    addMenu(mta: AbsMenu, name: string) {
        this.menuWheel.push(mta);
        this.options.push(name);
    }

    mute(): void | Promise<void> {
        this.synth.pause();
    }
    jumpStart(): void | Promise<void> {
        const hash = this.hash.split("").join(" ").toUpperCase();
        this.speak("Main Menu " + hash)
        this.d1("Main Menu");
        this.d2("Main Menu");
        this.d3("Hash " + this.hash.toUpperCase());
    }
    enter(): void | Promise<void> {
        this.speak("Main Menu ")
        this.d1("Main Menu");
        this.d2("Main Menu");
        this.d3("Hash " + this.hash.toUpperCase());
    }
    up2(): void | Promise<void> {
        this.speak("Boss Key")
    }
    center(): void | Promise<void> {
        this.enterHelper(this.menuWheel[this.selected]);
    }
    leave(): void | Promise<void> {
        const now = new Date();
        const withPmAm = now.toLocaleTimeString('en-US', {
            // en-US can be set to 'default' to use user's browser settings
            hour: '2-digit',
            minute: '2-digit',
        });
        const clean = `It's ${withPmAm}`;
        this.d3(`⌛: ${withPmAm}`);
        this.speak(clean);
    }
}