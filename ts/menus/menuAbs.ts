/// <reference lib="dom" />
import { deferred } from "https://deno.land/std@0.140.0/async/mod.ts";
type vpv = void | Promise<void>
export abstract class Menu {
    synth: SpeechSynthesis;
    menuStack: Array<Menu>;
    constructor(globalSynth: SpeechSynthesis, menuStack: Array<Menu>) {
        this.synth = globalSynth;
        this.menuStack = menuStack;
    }
    d1(text: string) {
        document.getElementById("Output1")!.innerHTML = text
    }
    d2(text: string) {
        document.getElementById("Output2")!.innerHTML = text
    }
    d3(text: string) {
        document.getElementById("Output3")!.innerHTML = text
    }
    speak(text: string): Promise<void> {
        const p = deferred<void>();
        this.synth.cancel();
        this.synth.resume();
        const utterance = new SpeechSynthesisUtterance(text);
        utterance.onend = () => p.resolve();
        this.synth.speak(utterance);
        return p;
    }
    jumpStart(): vpv {
        this.enter();
    }
    // deno-lint-ignore no-explicit-any
    abstract enter(...args: any): vpv;
    // deno-lint-ignore no-explicit-any
    enterHelper(menu: Menu, ...args: any) {
        this.menuStack.push(menu);
        this.menuStack[this.menuStack.length - 1].enter(...args);
    }
    // deno-lint-ignore no-explicit-any
    abstract up1(...args: any): vpv;
    // deno-lint-ignore no-explicit-any
    abstract up2(...args: any): vpv;
    // deno-lint-ignore no-explicit-any
    abstract down1(...args: any): vpv;
    // deno-lint-ignore no-explicit-any
    abstract down2(...args: any): vpv;
    // deno-lint-ignore no-explicit-any
    abstract center(...args: any): vpv;
    // deno-lint-ignore no-explicit-any
    abstract leave(...args: any): vpv;
    // deno-lint-ignore no-explicit-any
    protected leaveHelper(...args: any) {
        this.synth.cancel();
        this.menuStack.pop();
        this.menuStack[this.menuStack.length - 1].enter(...args);
    }
    abstract mute(): vpv;
}