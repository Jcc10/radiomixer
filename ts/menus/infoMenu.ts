import { Menu as AbsMenu } from "./menuAbs.ts";
import { getBattery, BatteryManager } from "../batteryShim.ts";
function secondsToHms(d: number) {
    d = Number(d);
    const h = Math.floor(d / 3600);
    const m = Math.floor(d % 3600 / 60);
    const s = Math.floor(d % 3600 % 60);

    const hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
    const mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
    const sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    return hDisplay + mDisplay + sDisplay;
}

export class Menu extends AbsMenu {
    battery?: BatteryManager;
    timeStart: number = Date.now();
    constructor(globalSynth: SpeechSynthesis, menuStack: Array<AbsMenu>) {
        super(globalSynth, menuStack);
        getBattery().then(bm => {
            this.battery = bm;
        });
    }
    time() {
        const now = new Date();
        const withPmAm = now.toLocaleTimeString('en-US', {
            // en-US can be set to 'default' to use user's browser settings
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit',
        });
        return withPmAm;
    }
    batLevel() {
        if (this.battery) {
            const bat10 = Math.round(this.battery.level * 100_00);
            const bat0 = bat10 / 100
            return `${bat0}%`
        } else {
            return `Unknown %`
        }
    }
    enter(): void | Promise<void> {
        const time = this.time();
        let clean = `It's ${time}`;
        this.d3(`⌛: ${time}`);
        if (this.battery) {
            const bat = this.batLevel();
            clean = `It's ${time} and the battery is ${bat} charged.`;
            this.d3(`⌛: ${time}<br />🔋: ${bat}`);
        }
        this.speak(clean)
        this.d1("Info");
    }
    up1(): void | Promise<void> {
        const time = this.time();
        const clean = `It's ${time}`;
        this.speak(clean)
        this.d3(`⌛: ${time}`);
    }
    up2(): void | Promise<void> {
        const delta = Date.now() - this.timeStart;
        const deltaSec = delta / 1000;
        this.speak(`${secondsToHms(deltaSec)} elapsed`);
    }
    down1(): void | Promise<void> {
        const clean = `Battery is ${this.batLevel()} charged.`;
        this.speak(clean)
        this.d3(`🔋: ${this.batLevel()}`);
    }
    down2(): void | Promise<void> {
        this.speak("Started");
        this.timeStart = Date.now();
    }
    center(): void | Promise<void> {
        const time = this.time();
        let clean = `It's ${time}`;
        this.d3(`⌛: ${time}`);
        if (this.battery) {
            const bat = this.batLevel();
            clean = `It's ${time} and the battery is ${bat} charged.`;
            this.d3(`⌛: ${time}<br />🔋: ${bat}`);
        }
        this.speak(clean)
    }
    leave(): void | Promise<void> {
        this.leaveHelper();
    }
    mute(): void | Promise<void> {
        this.synth.pause();
    }

}