import { Menu as AbsMenu } from "./menuAbs.ts";
import { RotateMenuAbs } from "./selectionMenusAbs.ts";
import type { Player } from "../player.ts";

abstract class PlayerMenus extends AbsMenu {
    protected player: Player;
    constructor(globalSynth: SpeechSynthesis, menuStack: Array<AbsMenu>, player: Player) {
        super(globalSynth, menuStack);
        this.player = player;
    }
    async announce(): Promise<void> {
        this.player.hold();
        this.d2(this.player.display);
        await this.speak(this.player.display)
        this.player.restore();
    }
    async next(trackID?: number) {
        this.player.hold();
        this.player.next(trackID);
        await this.announce();
        if (this.player.audioElem.readyState >= 3) {
            this.player.restore();
        } else {
            const cpt = () => {
                this.player.restore();
                this.player.audioElem.removeEventListener("canplaythrough", cpt);
                this.player.audioElem.removeEventListener("abort", cpt);
            }
            const abt = () => {
                this.player.audioElem.removeEventListener("canplaythrough", cpt);
                this.player.audioElem.removeEventListener("abort", cpt);
            }
            this.player.audioElem.addEventListener("canplaythrough", cpt);
            this.player.audioElem.addEventListener("abort", abt);
        }
    }
    async last() {
        this.player.last();
        await this.announce();
        this.player.play();
    }
    center(): void | Promise<void> {
        this.announce();
    }
    mute(): void | Promise<void> {
        this.player.delta(-5);
        this.player.pause();
    }
}
export class Menu extends PlayerMenus {
    skip: Skip;
    track: Track;
    volume = 5;
    constructor(globalSynth: SpeechSynthesis, menuStack: Array<AbsMenu>, player: Player) {
        super(globalSynth, menuStack, player);
        this.player.ended = () => { this.next() };
        this.player.error = () => { this.next() };
        this.skip = new Skip(globalSynth, menuStack, player);
        this.track = new Track(globalSynth, menuStack, player);
    }
    enter(internal?: boolean, skipAnn?: boolean): void | Promise<void> {
        if (internal && !skipAnn) {
            this.speak("Vol.")
            return;
        }
        if (internal && skipAnn) {
            return;
        }
        if (this.player.history.length <= 0) {
            this.next()
        } else {
            this.announce();
        }
        this.d1("Music");
        this.d3(`Vol: ${Math.round(this.player.volume * 10)}`);
    }
    async up1(): Promise<void> {
        this.volume += 1;
        this.player.volume = this.volume / 10;
        if (this.volume >= 10) {
            this.volume = 10
            await this.speak("Max");
        } else if (this.volume <= 0) {
            this.volume = 0;
            await this.speak("Mute");
        } else {
            this.speak(`${this.volume}`);
        }
        this.d3(`Vol: ${Math.round(this.player.volume * 10)}`);
    }
    up2(): void | Promise<void> {
        this.enterHelper(this.skip);
    }
    async down1(): Promise<void> {
        this.volume -= 1;
        this.player.volume = this.volume / 10;
        if (this.volume >= 10) {
            this.volume = 10
            await this.speak("Max");
        } else if (this.volume <= 0) {
            this.volume = 0;
            await this.speak("Mute");
        } else {
            this.speak(`${this.volume}`);
        }
        this.d3(`Vol: ${Math.round(this.player.volume * 10)}`);
    }
    down2(): void | Promise<void> {
        this.enterHelper(this.track);
    }
    leave(): void | Promise<void> {
        this.player.hold();
        this.leaveHelper();
    }

}

class Skip extends PlayerMenus {
    enter(): void | Promise<void> {
        this.speak("Seek");
        this.d3("Seek");
    }
    up1(): void | Promise<void> {
        const [now, total] = this.player.pos;
        this.d3(`(${now} + 10) / ${total}`)
        this.player.delta(10);
    }
    up2(): void | Promise<void> {
        const [now, total] = this.player.pos;
        this.d3(`(${now} + 30) / ${total}`)
        this.player.delta(30);
    }
    down1(): void | Promise<void> {
        const [now, total] = this.player.pos;
        this.d3(`(${now} - 10) / ${total}`)
        this.player.delta(-10);
    }
    down2(): void | Promise<void> {
        const [now, total] = this.player.pos;
        this.d3(`(${now} - 30) / ${total}`)
        this.player.delta(-30);
    }
    leave(): void | Promise<void> {
        this.leaveHelper(true);
    }
}

// deno-lint-ignore no-explicit-any
function generateKeys(map: Map<string, any>): Array<string> {
    const arr = [];
    for (const key of map.keys()) {
        arr.push(key);
    }
    return arr;

}

class Track extends PlayerMenus {
    albumSelect: AlbumSelect;
    trackSelect: TrackSelect;
    constructor(globalSynth: SpeechSynthesis, menuStack: Array<AbsMenu>, player: Player) {
        super(globalSynth, menuStack, player);
        this.albumSelect = new AlbumSelect(globalSynth, menuStack);
        this.trackSelect = new TrackSelect(globalSynth, menuStack);
    }
    async enter(internal?: boolean, album?: string, track?: string, historyID?: number): Promise<void> {
        if (internal) {
            if (track && album) {
                const id = this.player.albums.get(album)?.get(track);
                if (!id) {
                    if (!historyID) {
                        this.leave(true);
                        return;
                    }
                    if (historyID >= this.player.history.length) {
                        this.leave(true);
                        return;
                    }
                    await this.next(this.player.history[historyID]);
                    this.leave(true);
                    return;
                }
                await this.next(id);
                this.leave(true);
                return;
            } else if (!track && album) {
                const alMap = this.player.albums.get(album);
                let alArr: Array<string> | null = null;
                if (alMap) {
                    alArr = generateKeys(alMap);
                }
                if (album == "History") {
                    alArr = [];
                    for (const id of this.player.history) {
                        alArr.push(this.player.tracks[id].name);
                    }
                }
                if (!alArr) {
                    this.leave(true);
                    return;
                }
                this.enterHelper(this.trackSelect, alArr, album);
                return;
            } else if (!track && !album) {
                this.leave(true);
                return;
            }
        }
        this.speak("Track");
        this.d3("Track");
    }
    up1(): void | Promise<void> {
        this.player.hold();
        this.enterHelper(this.albumSelect, generateKeys(this.player.albums));
    }
    async up2(): Promise<void> {
        await this.speak("Skip")
        await this.next();
        this.d3(`Next Track`)
        this.leave(true);
    }
    down1(): void | Promise<void> {
        this.player.delta(-Infinity);
        this.d3(`Restart`)
    }
    down2(): void | Promise<void> {
        this.last();
        this.d3(`Last Track`);
        this.leave(true);
    }
    leave(skipAnn?: boolean): void | Promise<void> {
        this.leaveHelper(true, skipAnn);
    }
}

class AlbumSelect extends RotateMenuAbs {
    select = 0;
    constructor(globalSynth: SpeechSynthesis, menuStack: Array<AbsMenu>) {
        super(globalSynth, menuStack);
        this.options = ["Unconfigured"];
    }

    async enter(albums: Array<string>): Promise<void> {
        //this.options = ["History", ...albums];
        this.options = [...albums];
        this.options = this.options.sort();
        super.enter();
        this.d2(this.selText());
        await this.speak("Select Album");
        await this.speak(this.selText())
    }
    async up2(): Promise<void> {
        await this.speak("Method not implemented.");
    }
    center(): void | Promise<void> {
        this.leaveHelper(true, this.selText());
    }
    leave(): void | Promise<void> {
        this.leaveHelper(true);
    }
    mute(): void | Promise<void> {
        super.mute();
        this.menuStack[this.menuStack.length - 2].mute();
    }
}

class TrackSelect extends RotateMenuAbs {
    select = 0;
    albumName = "Unknown";
    constructor(globalSynth: SpeechSynthesis, menuStack: Array<AbsMenu>) {
        super(globalSynth, menuStack);
        this.options = ["Unconfigured"];
    }
    async enter(tracks: Array<string>, albumName: string): Promise<void> {
        this.albumName = albumName;
        this.options = tracks;
        this.options = this.options.sort();
        super.enter();
        this.d2(this.selText());
        this.d3(albumName);
        await this.speak("Select Track");
        await this.speak(this.selText())
    }
    async up2(): Promise<void> {
        await this.speak("Method not implemented.");
    }
    center(): void | Promise<void> {
        if (this.albumName == "History") {
            this.leaveHelper(true, this.albumName, this.selText(), this.select);
        } else {
            this.leaveHelper(true, this.albumName, this.selText());
        }
    }
    leave(): void | Promise<void> {
        this.leaveHelper(true);
    }
    mute(): void | Promise<void> {
        super.mute();
        this.menuStack[this.menuStack.length - 2].mute();
    }
}