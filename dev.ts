import { bundleAndSave } from "./build.ts"
try {
    await bundleAndSave();
} catch (_e) {
    console.warn("Error preforming bundle!");
}

const watcher = Deno.watchFs("./ts/");
for await (const event of watcher) {
    if (event.kind == "modify" || event.kind == "remove" || event.kind == "create") {
        try {
            await bundleAndSave();
        } catch (_e) {
            console.warn("Error preforming bundle!");
        }
    }
}