import * as emit from "https://deno.land/x/emit@0.0.1/mod.ts";
import { createHash } from "https://deno.land/std@0.80.0/hash/mod.ts";
export async function bundleAndSave() {
  await bundleMain();
  await bundleSW();
}

async function bundleMain() {
  const url = new URL("./ts/main.ts", import.meta.url);
  const emited = await emit.bundle(url.href);
  await Deno.writeTextFile("./js/main.js", emited.code);
  const hash = createHash("md5").update(emited.code).toString();
  const shortHash = hash.substring(hash.length - 5);
  console.log(`Bundled hash ${shortHash}`)
  await Deno.writeTextFile("./hash", shortHash);
}

async function bundleSW() {
  const url = new URL("./sw.ts", import.meta.url);
  const emited = await emit.bundle(url.href);
  await Deno.writeTextFile("./sw.js", emited.code);
}

if (import.meta.main) {
  console.log("Bundeling...")
  await bundleAndSave();
  console.log("Bundled.")
}