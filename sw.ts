/// <reference lib="webworker" />
// interface FetchEvent extends Event {
//     request: Request;
//     respondWith: (response: Response | Promise<Response>) => void;
// }
const appCacheName = "radio_mixer";
const mediaCacheName = "media";
const filesToCache = [
    "/",
    "/index.html",
    "/css/style.css",
    "/js/main.js",
    "/hash",
];

let appCache: Cache | null = null;
let mediaCache: Cache | null = null;

/* Start the service worker and cache all of the app's content */
// deno-lint-ignore no-explicit-any
self.addEventListener("install", function (event: any) {
    const e = event as ExtendableEvent;
    e.waitUntil(async () => {
        appCache = await caches.open(appCacheName);
        await appCache.addAll(filesToCache);
        mediaCache = await caches.open(mediaCacheName);
        return;
    });
});

function stratageyChoice(request: Request) {
    const path = new URL(request.url);
    if (path.pathname.startsWith("/media/")) {
        return cacheOrLive(request, mediaCache);
    } else {
        return cacheOrLive(request, appCache);
    }
}

async function cacheOrLive(request: Request, cache: Cache | null) {
    const modifiedRequest = new Request(request, {
        credentials: "omit", // this is what removes cookies
    });
    let cacheP;
    if (cache == null) {
        cacheP = Promise.reject(undefined);
    } else {
        cacheP = cache?.match(modifiedRequest);
    }
    const liveP = fetch(modifiedRequest);
    const race = await Promise.any([cacheP, liveP]);
    if (race == undefined) {
        return liveP;
    } else {
        return race;
    }
}

/* Serve cached content when offline */
// deno-lint-ignore no-explicit-any
self.addEventListener("fetch", function (event: any) {
    const e = event as FetchEvent;
    const p = stratageyChoice(e.request);
    e.respondWith(p);
});
