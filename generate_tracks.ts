import { walk } from "https://deno.land/std@0.140.0/fs/mod.ts";
type track = { src: string, name: string, by: string, skip?: true }
const folder = Deno.args[0];
const tracks: Array<track> = [];
const banned = [
    ".ts",
    ".js",
    ".jpg",
    ".name"
]
let by = ""
try {
    by = await Deno.readTextFile(`${folder}/name.name`);
    console.log(`Album name is ${by}`);
} catch (_e) {
    console.error("Couldn't find name file.");
    Deno.exit(1);
}
let longest = 0;
for await (const entry of walk(folder, { maxDepth: 1, followSymlinks: true })) {
    let skip = false;
    if (entry.isDirectory) {
        continue;
    }
    for (const ban of banned) {
        if (entry.name.endsWith(ban)) {
            skip = true;
            break;
        }
    }
    if (skip) {
        continue;
    }
    const name = entry.name.split('.').slice(0, -1).join('.')
    if (name.length > longest) {
        longest = name.length;
    }
    const src = `/${entry.path}`;
    const track = { by, name, src }
    if (by == "" || name == "" || src == "") {
        continue;
    }
    tracks.push(track);
}
console.log(`Found ${tracks.length} tracks.`)
// Find Prefix
let x = 0;
top: while (x < longest) {
    let allMatch = true;
    let text: string | null = null;
    for (const track of tracks) {
        const { name } = track;
        if (text == null) {
            text = name.substring(0, x);
        } else {
            allMatch = name.startsWith(text);
        }
        if (!allMatch) {
            x--
            break top;
        }
    }
    x++;
}
if (x > 0) {
    console.log(`Prefix is ${x} long (${tracks[0].name.substring(0, x)})`);
    for (const id in tracks) {
        tracks[id].name = tracks[id].name.substring(x);
    }
} else {
    console.log("Couldn't find a prefix.");
}
// Replace Underscores with spaces.
for (const id in tracks) {
    tracks[id].name = tracks[id].name.replaceAll("_", " ");
}
await Deno.writeTextFile(`${folder}/tracks.ts`, `
type track = { src: string, name: string, by: string, skip?: true }
export const tracks: Array<track> = ${JSON.stringify(tracks, null, 2)};`)
console.log("done")